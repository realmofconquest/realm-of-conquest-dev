/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackveiled.roc.sql;

import com.blackveiled.roc.RealmOfConquest;
import com.blackveiled.roc.player.PlayerHero;

import java.sql.*;
import org.bukkit.Bukkit;

import org.bukkit.entity.Player;

public class Database {
    public Connection connection;
    public PreparedStatement Query;
    public ResultSet Results;
    public String Hostname;
    public String Schema;
    public String Username;
    public String Password;
    
    public Database() {
        this.Hostname = "localhost";
        this.Schema = "database";
        this.Username = "root";
        this.Password = "password";
    }
    
    public Connection getConnection() throws SQLException {
        this.connection = null;
        try {
        return this.connection = DriverManager.getConnection("jdbc:mysql://"+this.Hostname+":3307/"+this.Schema+"?allowMultiQueries=true", this.Username, this.Password);
        }
        catch (SQLException Exception)  {
            Exception.printStackTrace();
            return this.connection = null;
            
        }
    }
    public void getUserStats(PlayerHero Hero) throws SQLException {
        if(this.connection!=null) {
                this.Query = null;
                String Select = "SELECT `GOLD`, `STR`, `AGI`, `INT`, `VIT`, `RES` FROM `players` WHERE `USERNAME`='"+Hero.Player+"' LIMIT 0, 1;";
                this.Query = this.connection.prepareStatement(Select);
                this.Results = this.Query.executeQuery();
                while(this.Results.next()) {
                    Hero.GOLD = this.Results.getString("GOLD");
                    Hero.STR = this.Results.getInt("STR");
                    Hero.AGI = this.Results.getInt("AGI");
                    Hero.INT = this.Results.getInt("INT");
                    Hero.VIT = this.Results.getInt("VIT");
                    Hero.RES = this.Results.getInt("RES");
                }
                this.Query.close();
                this.Results.close();
        }
    }
    public void getUserClass(PlayerHero Hero) throws SQLException {
        if(this.connection!=null) {
                this.Query = null;
                String Select = "SELECT `CLASS` FROM `players` WHERE `USERNAME`='"+Hero.Player+"' LIMIT 0, 1;";
                this.Query = this.connection.prepareStatement(Select);
                this.Results = this.Query.executeQuery();
                while(this.Results.next()) {
                    Hero.CLASS = this.Results.getString("CLASS");
                }
                this.Query.close();
                this.Results.close();
        }
    }
    public int getRowCount(String table, String col, String equals) throws SQLException {
        if(this.connection!=null)   {
            this.Query = null;
            String Countfor = "SELECT COUNT(*) FROM `"+table+"` WHERE `"+col+"`='"+equals+"';";
            try {
            this.Query = this.connection.prepareStatement(Countfor);
            this.Results = this.Query.executeQuery();
            while(this.Results.next())  {
                return this.Results.getInt("COUNT(*)");
            }
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
            finally {
                this.Query.close();
            }
        }   else    {
            return -1;
        }
        return -1;
    }
    public int getRowCountAnd(String table, String col, String equals, String andCol, String andEquals) throws SQLException {
        if(this.connection!=null)   {
            this.Query = null;
            String Countfor = "SELECT COUNT(*) FROM `"+table+"` WHERE `"+col+"`='"+equals+"' AND `"+andCol+"`='"+andEquals+"';";
            try {
            this.Query = this.connection.prepareStatement(Countfor);
            this.Results = this.Query.executeQuery();
            while(this.Results.next())  {
                return this.Results.getInt("COUNT(*)");
            }
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
            finally {
                this.Query.close();
            }
        }   else    {
            return -1;
        }
        return -1;
    }    
    public void createUser(String username) throws SQLException  {
        if(this.connection!=null)   {
            this.Query = null;
            String insert = "INSERT INTO `players` (`USERNAME`) VALUES('"+username+"');";
            this.Query = this.connection.prepareStatement(insert);
            int rows = this.Query.executeUpdate();
            this.Query.close();
        }
        
    }
    public void userLogin(String username) throws SQLException  {
        if(this.connection!=null)   {
            this.Query = null;
            String IP = Bukkit.getPlayer(username).getAddress().getHostName();
            String insert = "INSERT INTO `players_online` (`USERNAME`, `IP`) VALUES('"
                    +username+"', '"
                    +IP+"');";
            this.Query = this.connection.prepareStatement(insert);
            this.Query.executeUpdate();
            this.Query.close();
        }
        
    }
    public void userLogout(String username) throws SQLException  {
        if(this.connection!=null)   {
            this.Query = null;
            String IP = Bukkit.getPlayer(username).getAddress().getHostName();
            String insert = "DELETE FROM `players_online` WHERE `USERNAME`='"+username+"';";
            this.Query = this.connection.prepareStatement(insert);
            this.Query.executeUpdate();
            this.Query.close();
        }
    
    }    
    public void clearPlayersOnline() throws SQLException    {
        if(this.connection!=null)   {
            this.Query = null;
            String Truncate = "TRUNCATE `players_online`;";
            this.Query = this.connection.prepareStatement(Truncate);
            this.Query.execute();
            this.Query.close();
        }
    }
    public void reloadPlayersOnline() throws SQLException   {
        if(this.connection!=null)   {
            this.Query = null;
            Player[] Players = Bukkit.getOnlinePlayers();
            try {
                for (Player Player : Players) {
                    String username = Player.getName();
                    String IP = Bukkit.getPlayer(username).getAddress().getHostName();
                    String insert = "INSERT INTO `players_online` (`USERNAME`, `IP`) VALUES('"
                            +username+"', '"
                            +IP+"');";
                    this.Query = this.connection.prepareStatement(insert);
                    this.Query.execute();
                    this.Query.close();
                }
            }
            catch (NullPointerException Exception)  {
            }
            finally {
            }
        }
    }
    public int getPlayersOnline() throws SQLException   {
        if(this.connection!=null)   {
            this.Query = null;
            String Select = "SELECT COUNT(*) FROM `players_online`";
            try {
            this.Query = this.connection.prepareStatement(Select);
            this.Results = this.Query.executeQuery();
            while(this.Results.next())  {
                return this.Results.getInt("COUNT(*)");
            }
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
            finally {
                this.Query.close();
                this.Results.close();
            }
        }
        return -1;
    }
    
}