package com.Blackveil.ÜberMMO;

import java.util.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Objective;

import org.bukkit.entity.Player;

import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.Inventory;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.entity.NPC;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.metadata.MetadataValue;
/**
 *
 * @author Blackveiled
 */
public class PlayerHeroes {
    public static Map<String, Integer> PlayerHeroes = new HashMap<String, Integer>();
    public static ArrayList<ArrayList> PlayerStats = new ArrayList<ArrayList>();
    public static ArrayList<Inventory> PlayerCharacterScreens = new ArrayList<Inventory>();
    public static ArrayList<Scoreboard> PlayerStatDisplay = new ArrayList<Scoreboard>();
    public static ArrayList<String> PlayerMetaData = new ArrayList<String>();
    
    public static int playerid = 0;
    
    public static void PlayerHeroLogin(Player p) throws SQLException{
        String player = p.getName();
        if(PlayerHeroes.containsKey(player)) {
            // do nothing, player data is already input into list
        }   else    {
            try {
                
                p.sendMessage(Notifier.formatEnforcer(true, "Welcome to the Realm of Conquest!"));
                World world = Bukkit.getWorld("world");
                
                ResultSet rs = MySQL.getUser(p);
                int Level = rs.getInt("Level");
                int Class = rs.getInt("Class");
                int Strength = rs.getInt("Strength");
                int Agility = rs.getInt("Agility");
                int Intelligence = rs.getInt("Intelligence");
                int Vitality = rs.getInt("Vitality");
                int Resilience = rs.getInt("Resilience");
                int StatPoints = rs.getInt("StatPoints");
                int Damage = rs.getInt("Damage");
                
                
                ArrayList<Integer> PlayerStatValues = new ArrayList<Integer>();
                PlayerStatValues.add(0, Level);
                PlayerStatValues.add(1, Class);
                PlayerStatValues.add(2, Strength);
                PlayerStatValues.add(3, Agility);
                PlayerStatValues.add(4, Intelligence);
                PlayerStatValues.add(5, Vitality);
                PlayerStatValues.add(6, Resilience);
                PlayerStatValues.add(7, StatPoints);
                PlayerStatValues.add(8, Damage);
                PlayerStatValues.add(9, 0); // Armor Stat Value Default
                PlayerStats.add(playerid, PlayerStatValues);
                getHelmetStats(p, "Add");
                getChestStats(p, "Add");
                getLeggingsStats(p, "Add");
                getBootsStats(p, "Add");
                ItemStack stack = p.getItemInHand();
            if(stack.getType().equals(Material.DIAMOND_SWORD) ||
               stack.getType().equals(Material.IRON_SWORD) ||
               stack.getType().equals(Material.GOLD_SWORD) ||
               stack.getType().equals(Material.WOOD_SWORD) ||
               stack.getType().equals(Material.DIAMOND_AXE) ||
               stack.getType().equals(Material.IRON_AXE) ||
               stack.getType().equals(Material.GOLD_AXE) ||
               stack.getType().equals(Material.WOOD_AXE)     ){getanyItemStats(p, "Add", p.getItemInHand());}
                
                p.setMaxHealth(Vitality * 2);
                p.setHealthScaled(true);
                
                PlayerHeroes.put(player, playerid);
                world = null;
                playerid++;
            } catch (NullPointerException ex) { }
            finally {
                Main.Connection.close();
            }
             // End of Catch
        }
    }
    
////////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
///   GET STATS GET STATS GET STATS GET STATS GET STATS GET STATS GET STATS GET STATS GET STAT////

    
    public static Integer getLevel(Player p) {
        if(PlayerHeroes.containsKey(p.getName())) {
            int id = PlayerHeroes.get(p.getName());
            ArrayList<Integer> Stats = PlayerStats.get(id);
            int Level = Stats.get(0);
            return Level;
            
        }
        return 0;
    }
    public static Integer setLevel(Player p, String i) {
        if(PlayerHeroes.containsKey(p.getName())) {
            int id = PlayerHeroes.get(p.getName());
            ArrayList<Integer> Stats = PlayerStats.get(id);
            //int Level = String.toInteger(i);
            
            return 0;
            
        }
        return 0;
    }    
   
    public static void postCharacterPage(Player p, Player requester) {
        if(PlayerHeroes.containsKey(p.getName())) {
            int id = PlayerHeroes.get(p.getName());
            ArrayList<Integer> Stats = PlayerStats.get(id);
            int Armor = Stats.get(9);
            int Damage = Stats.get(8);
            int Level = Stats.get(0);
            int Class = Stats.get(1);
            int Strength = Stats.get(2);
            int Agility = Stats.get(3);
            int Intelligence = Stats.get(4);
            int Vitality = Stats.get(5);
            int Resilience = Stats.get(6); 
            requester.sendMessage(ChatColor.WHITE+p.getName()+"'s Character Statistics");
            requester.sendMessage(ChatColor.RED+"Maximum Health: "+Integer.toString(Vitality*2)+" Armor: "+Armor+"(Take "+Integer.toString(Armor/4)+" less damage) / Damage: "+Damage);
            requester.sendMessage(ChatColor.GOLD+"Level: "+ChatColor.WHITE+""+Level+""+ChatColor.GOLD+" / Class: "+ChatColor.RED+"In Development");
            requester.sendMessage(ChatColor.GOLD+"Strength: "+ChatColor.WHITE+""+Strength+""+ChatColor.GOLD+" / Agility: "+ChatColor.WHITE+""+Agility);
            requester.sendMessage(ChatColor.GOLD+"Intelligence: "+ChatColor.WHITE+""+Intelligence+""+ChatColor.GOLD+" / Vitality: "+ChatColor.WHITE+""+Vitality);
            requester.sendMessage(ChatColor.GOLD+"Resilience: "+ChatColor.WHITE+""+Resilience);
        }
    }
    
    public static void getNewCharacterPage(String pn)    {
        if(PlayerHeroes.containsKey(pn)) {
            int id = PlayerHeroes.get(pn);
            Inventory CharacterScreen = PlayerCharacterScreens.get(id);
            
            Player p = Bukkit.getPlayer(pn);
            p.openInventory(CharacterScreen);
        }
    }
    
    public static void getHelmetStats(Player p, String Add)    {
        try {ItemStack stack = p.getInventory().getHelmet();
        if(p.getInventory().getHelmet().hasItemMeta())  {
            ItemMeta meta = stack.getItemMeta();
            if(meta.hasLore())   {
                getItemStats(meta.getLore(), p, Add);
            }
        }
        } catch (NullPointerException ex) {
            
        }
    }
    
    public static void getChestStats(Player p, String Add)    {
        try{ItemStack stack = p.getInventory().getChestplate();
        if(p.getInventory().getChestplate().hasItemMeta())  {
            ItemMeta meta = stack.getItemMeta();
            if(meta.hasLore())   {
                getItemStats(meta.getLore(), p, Add);
            }
        }
        } catch (NullPointerException ex) {
            
        }
    }

    public static void getLeggingsStats(Player p, String Add)    {
        try{ItemStack stack = p.getInventory().getLeggings();
        if(p.getInventory().getLeggings().hasItemMeta())  {
            ItemMeta meta = stack.getItemMeta();
            if(meta.hasLore())   {
                getItemStats(meta.getLore(), p, Add);
            }
        }
        } catch (NullPointerException ex) {
            
        }
    }
    
    public static void getBootsStats(Player p, String Add)    {
        try{ItemStack stack = p.getInventory().getBoots();
        if(p.getInventory().getBoots().hasItemMeta())  {
            ItemMeta meta = stack.getItemMeta();
            if(meta.hasLore())   {
                getItemStats(meta.getLore(), p, Add);
            }
        }
        } catch (NullPointerException ex) {
            
        }
    }
    
    public static void getanyItemStats(Player p, String Add, ItemStack item)    {
        try{ItemStack stack = item;
        if(stack.hasItemMeta())  {
            ItemMeta meta = stack.getItemMeta();
            if(meta.hasLore())   {
                getItemStats(meta.getLore(), p, Add);
            }
        }
        } catch (NullPointerException ex) {
            
        }
    }
    
    public static void getItemStats(List lore, Player p, String add)  {
        if(PlayerHeroes.containsKey(p.getName())){
                int id = PlayerHeroes.get(p.getName());
                ArrayList<Integer> Stats = PlayerStats.get(id);
                int Armor = Stats.get(9);
                int Damage = Stats.get(8);
                int Class = Stats.get(1);
                int Strength = Stats.get(2);
                int Agility = Stats.get(3);
                int Intelligence = Stats.get(4);
                int Vitality = Stats.get(5);
                int Resilience = Stats.get(6);
                for(int i = 0; i < lore.size(); i++)    {
                    String stat = lore.get(i).toString();
                    ChatColor.stripColor(stat);
                    int length = stat.length();
                    if(stat.endsWith("Armor")) {
                        stat = stat.substring(2, length - 6);
                        if(add.equalsIgnoreCase("Add")){ Armor = Armor + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Armor = Armor - Integer.parseInt(stat);}
                        Stats.remove(9);
                        Stats.add(9, Armor);
                    }
                    if(stat.endsWith("Damage")) {
                        stat = stat.substring(2, length - 7);
                        if(add.equalsIgnoreCase("Add")){ Damage = Damage + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Damage = Damage - Integer.parseInt(stat);}
                        Stats.remove(8);
                        Stats.add(8, Damage);
                    }
                    if(stat.endsWith("Strength")) {
                        stat = stat.substring(2, length - 9);
                        if(add.equalsIgnoreCase("Add")){ Strength = Strength + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Strength = Strength - Integer.parseInt(stat);}
                        Stats.remove(2);
                        Stats.add(2, Strength);
                    }
                    if(stat.endsWith("Agility")) {
                        stat = stat.substring(2, length - 8);
                        if(add.equalsIgnoreCase("Add")){ Agility = Agility + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Agility = Agility - Integer.parseInt(stat);}
                        Stats.remove(3);
                        Stats.add(3, Agility);
                    }
                    if(stat.endsWith("Intelligence")) {
                        stat = stat.substring(2, length - 13);
                        if(add.equalsIgnoreCase("Add")){ Intelligence = Intelligence + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Intelligence = Intelligence - Integer.parseInt(stat);}
                        Stats.remove(4);
                        Stats.add(4, Intelligence);
                    }
                    if(stat.endsWith("Vitality")) {
                        stat = stat.substring(2, length - 9);
                        if(add.equalsIgnoreCase("Add")){ Vitality = Vitality + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Vitality = Vitality - Integer.parseInt(stat);}
                        Stats.remove(5);
                        Stats.add(5, Vitality);
                    }
                    if(stat.endsWith("Resilience")) {
                        stat = stat.substring(2, length - 11);
                        if(add.equalsIgnoreCase("Add")){ Resilience = Resilience + Integer.parseInt(stat);}
                        else if (add.equalsIgnoreCase("Subtract")) { Resilience = Resilience - Integer.parseInt(stat);}
                        Stats.remove(6);
                        Stats.add(6, Resilience);
                    }
                    if(stat.startsWith("Class: ")) {
                        // To be implemented
                    }
                    
                    p.setMaxHealth(Vitality * 2);
                    p.setHealthScaled(true);
                    
                    PlayerStats.remove(id);
                    PlayerStats.add(id, Stats);
                }
                
            }
        
    }
    
    public static double getPlayerDamage(Player p) {
                int id = PlayerHeroes.get(p.getName());
                ArrayList<Integer> Stats = PlayerStats.get(id);
                int Damage = Stats.get(8);
                //int id2 = PlayerHeroes.get(v.getName());
                //ArrayList<Integer> StatsV = PlayerStats.get(id2);
                //int ArmorV = StatsV.get(9);
                return Damage;
                //double damageDealt = Damage-(ArmorV/4);
                //if(damageDealt>0)   {
                //    return damageDealt;
                //}   else{
                //    return 0;
                //}
}
    
    public static double getPlayerStrength(Player p) {
                int id = PlayerHeroes.get(p.getName());
                ArrayList<Integer> Stats = PlayerStats.get(id);
                int Strength = Stats.get(2);
                //int id2 = PlayerHeroes.get(v.getName());
                //ArrayList<Integer> StatsV = PlayerStats.get(id2);
                //int ArmorV = StatsV.get(9);
                return Strength;
                //double damageDealt = Damage-(ArmorV/4);
                //if(damageDealt>0)   {
                //    return damageDealt;
                //}   else{
                //    return 0;
                //}
}    
    
    public static String getPlayerClass(Player p) {
                int id = PlayerHeroes.get(p.getName());
                ArrayList<Integer> Stats = PlayerStats.get(id);
                int Class = Stats.get(1);
                String ClassName = "Trainee";
                if(Class==0) {
                    ClassName = "Trainee";
                }   else if(Class==1) {
                    ClassName = "Barbarian";
                }
                return ClassName;
}
    
    public static void addMenuItem(Inventory cinv, Material mat, String title) {
                ItemStack stack = new ItemStack(mat);
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName(title);
                stack.setItemMeta(meta);
                cinv.addItem(stack);
    }
    
    public static void playerHealth()  {
    ScoreboardManager manager = Bukkit.getScoreboardManager();
    Scoreboard board = manager.getNewScoreboard();
    board.registerNewObjective("showhealth", "health");
 
    Objective objective = board.getObjective("showhealth");
    objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
    objective.setDisplayName("❤");
 
    for(Player online : Bukkit.getOnlinePlayers()){
    online.setScoreboard(board);
    }
    }
}
