/**
 *
 * @author Blackveiled
 */
package com.blackveiled.roc.items;
import com.blackveiled.roc.sql.Database;
import java.sql.*;
import java.util.List;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class Builder {
    public ItemStack Stack = null;
    public ItemMeta Meta = null;
    public ItemFactory Factory = null;
    public Player Holder = null;
    public Database Database = new Database();
    public List<String> Lore = new ArrayList<String>();
    // STRINGS
    public Integer itemid = 0;
    public String ItemName = null;
    public String ItemType = null;
    public String Quality = null;
    public CharSequence FLAGS = null;
    public int DURA = 0;
    public int MAXDURA = 0;
    public String CUSTOMENCHANT = null;
    public String DESC = null;
    public int ITEMLEVEL = 0;
    public int LEVELREQ = 0;
    public int CLASSREQ = 0;
    public int MINDAMAGE = 0;
    public int MAXDAMAGE = 0;
    public int ARMOR = 0;
    public int STR = 0;
    public int AGI = 0;
    public int INT = 0;
    public int VIT = 0;
    public int RES = 0;
    public int VALUE = 0;
    
    public void getItemData(ItemStack stack, Player p) {
        Stack = stack;
        Holder = p;
        if(Stack.hasItemMeta()) {
            Meta = stack.getItemMeta();
            if(Meta.hasDisplayName()) {
                try {
                    String itemname = Meta.getDisplayName();
                    itemname = ChatColor.stripColor(itemname);
                    itemid = Integer.parseInt(itemname);
                    this.Database.getConnection();
                    if(this.Database.getRowCount("items", "ITEMNAME", itemname)==0) {
                        Lore.add(ChatColor.RED+""+ChatColor.BOLD+"This item does not exist!");
                        Meta.setLore(Lore);
                        Stack.setItemMeta(Meta);
                        Lore.clear();
                        this.Database.connection.close();
                    }
                    else {
                        if(this.Database.getRowCountAnd("itemsplayerdata", "USERNAME", Holder.getName(), "ITEMNAME", itemname)>0)  {
                            // Then gather player item data
                            String Query = "SELECT `DURA`, `ENCHANT` FROM `itemsplayerdata` WHERE `USERNAME`='"+Holder.getName()+"' AND `ITEMNAME`='"+itemname+"' LIMIT 0,1;";
                            this.Database.Query = this.Database.connection.prepareStatement(Query);
                    }   else    {
                            // Then create player item data & generate new item text
                            String Query = "SELECT * FROM `items` WHERE `ITEMNAME`='"+itemname+"';";
                            this.Database.getConnection();
                            this.Database.Query = this.Database.connection.prepareStatement(Query);
                            this.Database.Results = this.Database.Query.executeQuery();
                            while(this.Database.Results.next()) {
                                this.ItemType = this.Database.Results.getString("ITEMTYPE");
                                this.Quality = this.Database.Results.getString("QUALITY");
                                this.DESC = this.Database.Results.getString("DESC");
                                this.FLAGS = this.Database.Results.getString("FLAGS");
                                this.ITEMLEVEL = this.Database.Results.getInt("ITEMLEVEL");
                                this.LEVELREQ = this.Database.Results.getInt("LEVELREQ");
                                this.CLASSREQ = this.Database.Results.getInt("CLASSREQ");
                                this.MINDAMAGE = this.Database.Results.getInt("MINDAMAGE");
                                this.MAXDAMAGE = this.Database.Results.getInt("MAXDAMAGE");
                                this.ARMOR = this.Database.Results.getInt("ARMOR");
                                this.STR = this.Database.Results.getInt("STR");
                                this.AGI = this.Database.Results.getInt("AGI");
                                this.INT = this.Database.Results.getInt("INT");
                                this.VIT = this.Database.Results.getInt("VIT");
                                this.RES = this.Database.Results.getInt("RES");
                                this.VALUE = this.Database.Results.getInt("VALUE");
                            }
                            this.constructItemLore();
                            this.Database.connection.close();
                        }
                    }
                }
                catch (SQLException Exception)  {
                    Exception.printStackTrace();
                }
            }
        }
        else    
            {
            ///////////////////////////////////////////////////////////////////////
            //
            // Lets add custom lore for default Minecraft Items
            //
            ///////////////////////////////////////////////////////////////////////
            String itemname = Stack.getType().toString();
            itemname = ChatColor.stripColor(itemname);
            Meta = Stack.getItemMeta();
            try {
                this.Database.getConnection();
                if(this.Database.getRowCount("items", "ITEMNAME", itemname)<=0) {
                List<String> lore = new ArrayList<String>();
                lore.add("");
                lore.add(ChatColor.RED+"This item does not exist!");
                lore.add("");
                try {
                    Meta.setLore(lore);
                    Stack.setItemMeta(Meta);
                }
                catch (NullPointerException Exception){/*do nothing*/}
                this.Database.connection.close();
                
            }   else    {
                    ///////////////////////////////////////////////////////////////////////
                    //
                    // If normal MC item exists in DB, lets generate the lore
                    //
                    ///////////////////////////////////////////////////////////////////////   
                    String Query = "SELECT * FROM `items` WHERE `ITEMNAME`='"+itemname+"';";
                    this.Database.getConnection();
                    this.Database.Query = this.Database.connection.prepareStatement(Query);
                    this.Database.Results = this.Database.Query.executeQuery();
                    while(this.Database.Results.next()) {
                        this.ItemType = this.Database.Results.getString("ITEMTYPE");
                        this.FLAGS = this.Database.Results.getString("FLAGS");
                        this.Quality = this.Database.Results.getString("QUALITY");
                        this.DESC = this.Database.Results.getString("DESC");
                        this.ITEMLEVEL = this.Database.Results.getInt("ITEMLEVEL");
                        this.LEVELREQ = this.Database.Results.getInt("LEVELREQ");
                        this.CLASSREQ = this.Database.Results.getInt("CLASSREQ");
                        this.MINDAMAGE = this.Database.Results.getInt("MINDAMAGE");
                        this.MAXDAMAGE = this.Database.Results.getInt("MAXDAMAGE");
                        this.ARMOR = this.Database.Results.getInt("ARMOR");
                        this.STR = this.Database.Results.getInt("STR");
                        this.AGI = this.Database.Results.getInt("AGI");
                        this.INT = this.Database.Results.getInt("INT");
                        this.VIT = this.Database.Results.getInt("VIT");
                        this.RES = this.Database.Results.getInt("RES");
                        this.VALUE = this.Database.Results.getInt("VALUE");
                    }
                    this.constructItemLore();
                    this.Database.connection.close();
                    }
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
        }
    }
    
    public void constructItemLore() {
                    if(this.Quality.equalsIgnoreCase("Common")) {
                            this.Quality = ChatColor.RESET+"Common";
                        }   else if(this.Quality.equalsIgnoreCase("Uncommon"))  {
                            this.Quality = ChatColor.GREEN+"Uncommon";
                        }   else if(this.Quality.equalsIgnoreCase("Rare"))  {
                            this.Quality = ChatColor.BLUE+"Rare";
                        }   else if(this.Quality.equalsIgnoreCase("Epic"))  {
                            this.Quality = ChatColor.LIGHT_PURPLE+"Epic";
                        }   else if(this.Quality.equalsIgnoreCase("Legendary")) {
                            this.Quality = ChatColor.GOLD+"Legendary";
                        }
                    List<String> lore = new ArrayList<String>();
                    lore.add(this.Quality+" "+this.ItemType);
                    lore.add(ChatColor.DARK_GRAY+"Item Level "+Integer.toString(this.ITEMLEVEL));
                    lore.add("");
                    int end = 0;
                    if(this.DESC != null)   {
                    if(this.DESC.length() >= 25 && this.DESC.length() <= 50) {
                        lore.add(ChatColor.RESET+this.DESC.substring(0, 25));
                        lore.add(ChatColor.RESET+this.DESC.substring(25, this.DESC.length()));
                        
                    }   else if(this.DESC.length() >= 50 && this.DESC.length() <= 75)  {
                            lore.add(ChatColor.RESET+this.DESC.substring(0, 25));
                            lore.add(ChatColor.RESET+this.DESC.substring(25, 50));
                            lore.add(ChatColor.RESET+this.DESC.substring(50, this.DESC.length()));
                        }
                        else if(this.DESC.length() >= 75 && this.DESC.length() <= 100)  {
                            lore.add(ChatColor.RESET+this.DESC.substring(0, 25));
                            lore.add(ChatColor.RESET+this.DESC.substring(25, 50));
                            lore.add(ChatColor.RESET+this.DESC.substring(50, 75));
                            lore.add(ChatColor.RESET+this.DESC.substring(75, this.DESC.length()));
                        }
                        else if(this.DESC.length() <25) {
                            lore.add(ChatColor.RESET+this.DESC);
                        }
                    }
                    if(this.LEVELREQ>0)    {
                        lore.add("");
                        lore.add(ChatColor.GRAY+"Requires Level: "+Integer.toString(this.LEVELREQ));
                    }
                    lore.add("");
                    lore.add(ChatColor.YELLOW+"Sell Value: "+this.VALUE+"G");
                    try {
                        Meta.setLore(lore);
                        Stack.setItemMeta(Meta);
                    }
                    catch (NullPointerException Exception){/*do nothing*/}
    }
} 
