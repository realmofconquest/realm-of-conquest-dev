package com.blackveiled.roc.items;
import com.blackveiled.roc.items.Builder;
import java.sql.*;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ItemListener implements Listener {
    public Builder Builder = new Builder();
    
    @EventHandler
    public void playerSwitchItemInHand(PlayerItemHeldEvent event) {
        this.Builder.getItemData(event.getPlayer().getItemInHand(), event.getPlayer());
    }
}
