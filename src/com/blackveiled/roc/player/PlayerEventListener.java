package com.blackveiled.roc.player;
import com.blackveiled.roc.RealmOfConquest;
import com.blackveiled.roc.sql.*;
import com.blackveiled.roc.player.PlayerHero;

import java.util.HashMap;
import java.util.Map;
import java.sql.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Player;
import org.bukkit.ChatColor;

import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerEventListener implements Listener {
    
    protected Plugin plugin;
    protected Database Database = new Database();
    public Map<String, PlayerHero> PlayerHeroes = new HashMap<String, PlayerHero>();
    
    public PlayerEventListener(Plugin plugin) {
        this.plugin = plugin;
        try {
        this.Database.getConnection();
        }
        catch (SQLException Exception)  {
            Exception.printStackTrace();
        }
    }
    
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        event.setMessage(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
        event.setFormat(player.getDisplayName() + ": " + event.getMessage().replace("%", "%%"));
    }
    
    @EventHandler
    public void PlayerJoinEvent (PlayerJoinEvent event) throws SQLException {
        Player player = event.getPlayer();
        if(Database.connection==null)   {
            this.Database.getConnection();
        }   else {
        if(this.Database.getRowCount("players", "USERNAME", player.getName())==0)   {
                this.Database.createUser(player.getName());
            }
        String Query = "SELECT `LEVEL`, `CLASS`,`FLAG` FROM `players` WHERE `USERNAME`='"+player.getName()+"' LIMIT 0, 1";
        Database.Query = Database.connection.prepareStatement(Query);
        Database.Results = Database.Query.executeQuery();
        while(Database.Results.next())  {
            event.setJoinMessage(ChatColor.AQUA
            +"[Lv"+Integer.toString(Database.Results.getInt("LEVEL"))
            +" "
            +ChatColor.DARK_AQUA
            +Database.Results.getString("CLASS")
            +"] "
            +ChatColor.YELLOW
            +""
            +ChatColor.BOLD
            +""+player.getName()
            +""+ChatColor.GOLD
            +" has entered the Realm!");
            
            //////////////////////////////////////////////////////
            
            //////////////////////////////////////////////////////
            event.getPlayer().setDisplayName(ChatColor.AQUA
            +"[Lv"+Integer.toString(Database.Results.getInt("LEVEL"))
            +" "
            +ChatColor.DARK_AQUA
            +""
            +Database.Results.getString("CLASS")
            +"]"
            +ChatColor.YELLOW
            +" "
            +event.getPlayer().getName()
            +""
            +ChatColor.RESET);
            
        }
        Database.Results.close();
        Database.Query.close();
        
        playerLogin(player);
        }
    }
        
    
    
    @EventHandler
    public void PlayerQuitEvent (PlayerQuitEvent event) throws SQLException {
        Player player = event.getPlayer();
        playerLogout(player);
    }
    
    public void playerLogin(Player p) throws SQLException {
        if(this.Database.connection==null)   {
            this.Database.getConnection();
        }
        this.Database.userLogin(p.getName());
        p.sendMessage(ChatColor.YELLOW+" Welcome to the "
                +ChatColor.AQUA
                +"Realm of Conquest"
                +ChatColor.YELLOW
                +" Alpha Test! There are currently "
                +ChatColor.GOLD
                +""+Integer.toString(Database.getPlayersOnline())
                +ChatColor.RESET+ChatColor.YELLOW
                +" players online!"
                +" View our community website, post suggestions, feedback, and more at "
                +ChatColor.AQUA+""+ChatColor.UNDERLINE
                +"www.blackveiled.com"
                +ChatColor.YELLOW
                +"!  Thanks for contributing to the project!");
        }
    
    public void playerLogout(Player p) throws SQLException {
        if(this.Database.connection==null)   {
            this.Database.getConnection();
        }
        this.Database.userLogout(p.getName());
    }    
    
}
