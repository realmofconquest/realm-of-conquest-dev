package com.blackveiled.roc.player;

import com.blackveiled.roc.sql.*;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;

public class PlayerHero {
    public String Player;
    public String KINGDOM;
    public String GUILD;
    public double HEALTH;
    public double MAXHEALTH;
    public double DAMAGE = 2;
    public int ARMOR = 0;
    public int LEVEL = 1;
    public String GOLD = "00000";
    public int STR = 10;
    public int AGI = 10;
    public int INT = 10;
    public int VIT = 10;
    public int RES = 10;
    public int STATPOINTS = 0;
    public String CLASS = "Novice";
    public String PrimarySkill = "None";
    public String SecondarySkill = "None";
    public int PrimaryLevel = 0;
    public int SecondaryLevel = 0;
    
    
    public PlayerHero(Player p) {
        this.Player = p.getName();
        Bukkit.broadcastMessage("Building character data for "+this.Player);
        Database Database = new Database();
        try {
        Database.getConnection(); 
        int count = Database.getRowCount("players", "USERNAME", "'"+p.getName()+"'");
        Bukkit.broadcastMessage("Found: "+Integer.toString(count)+" results");
        if(count==0) {
            // Let's create a new user account.
            Database.createUser(this.Player);
        }            
        //Database.UpdateUserStats(this);
        //Bukkit.broadcastMessage("Stats: "+this.GOLD+this.STR+this.AGI+this.INT+this.VIT+this.RES);
        }
        catch (SQLException Exception) {
            Exception.printStackTrace();
            try {
                this.finalize();
            } 
            catch (Throwable ex) {
                Logger.getLogger(PlayerHero.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        finally {
        if(Database.connection!=null)   {
            try {
            Database.connection.close();
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
        }
        }
    }
    
    
    public Player getPlayer() {
         if (Bukkit.getPlayer(this.Player) != null)
            {
        return Bukkit.getPlayer(this.Player);
            }
        return null;
    }  
    
    public String getPlayerClass() {
        return this.CLASS;
    }
    
    public double getHealth() {
        return this.getPlayer().getHealth();
    }
    public double getMaxHealth() {
        return this.VIT*2;
    }    
    public void setMaxHealth() {
        this.getPlayer().setMaxHealth(this.getMaxHealth());
        this.getPlayer().setHealthScaled(true);
    }
    
    public int getArmor() {
        return this.ARMOR;
    }
    public int getLevel() {
        return this.LEVEL;
    }
    public int getSTR() {
        return this.STR;
    }
    public int getAGI() {
        return this.AGI;
    }
    public int getINT() {
        return this.INT;
    }
    public int getVIT() {
        return this.VIT;
    }
    public int getRES() {
        return this.RES;
    }
    public String getGold() {
        String Gold = this.GOLD.substring(0, this.GOLD.length()-5);
        return Gold;
    }
    public String getSilver() {
        String Silver = this.GOLD.substring(this.GOLD.length()-4, this.GOLD.length()-2);
        return Silver;
    }
    public String getCopper() {
        String Copper = this.GOLD.substring(this.GOLD.length()-2, this.GOLD.length());
        return Copper;
    }
   
    public void setArmor(int i) {
        this.ARMOR = i;
    }
    public void setLevel(int i) {
        this.LEVEL = i;
    }
    public void setSTR(int i) {
        this.STR = i;
    }
    public void setAGI(int i) {
        this.AGI = i;
    }
    public void setINT(int i) {
        this.INT = i;
    }
    public void setVIT(int i) {
        this.VIT = i;
    }
    public void setRES(int i) {
        this.RES = i;
    }
}
