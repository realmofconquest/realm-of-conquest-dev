package com.blackveiled.roc.extra;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import org.bukkit.event.block.SignChangeEvent;

public class ColorSigns implements Listener {
    
    @EventHandler
    public void ColorSigns (SignChangeEvent event){
        int length = event.getLines().length;
        for(int i = 0; i < length; i++)    {
            String text = ChatColor.translateAlternateColorCodes('&', event.getLine(i));
            event.setLine(i, text);
        }
    }
}

