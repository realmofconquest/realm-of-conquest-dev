/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackveiled.roc.commands;

import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import com.blackveiled.roc.sql.Database;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import java.io.File;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Sami
 */
public class tp implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
        if(s instanceof Player) {
            Player p = (Player) s;
            String playerName = p.getName();
            ///////////////////////////////////////////////////
            //////
            //////  USAGE:              /tp <player> <target>
            //////  PURPOSE:            To implement a database powered Warp system.`
            //////  PERMISSION-NODE:    roc.warp, roc.setwarp, roc.delwarp
            //////
            //////////////////////////////////////////////////
            if(cmd.getName().equalsIgnoreCase("tp")) { 
                if(args.length >0) {
                   
                }   
                else    
                { // Display "TP" Help Commands
                    p.sendMessage("");
                    p.sendMessage(ChatColor.YELLOW+"=============== "+ChatColor.RED+""+ChatColor.BOLD+"[ROC] "+ChatColor.RESET+ChatColor.UNDERLINE+"Teleportation System"+ChatColor.YELLOW+" =============== ");
                    p.sendMessage("");
                    p.sendMessage(ChatColor.GRAY+"Command Usage: "+ChatColor.GOLD+"/tp <player> <target>");
                }
            }
        }   else    {
            return false;
        }
        return true;
    }
}
