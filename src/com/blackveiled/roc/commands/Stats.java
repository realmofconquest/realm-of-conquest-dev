/**
 *
 * @author Blackveiled
 */
package com.blackveiled.roc.commands;

import org.bukkit.Bukkit;
import com.blackveiled.roc.sql.Database;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import java.io.File;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;


public class Stats implements CommandExecutor {
    protected Plugin plugin;
    protected Database Stats = new Database();
    public String Message = null;
    public int rows = 0;
    
    public Stats(Plugin plugin) {
        this.plugin = plugin;
        try {
        this.Stats.getConnection();
        }
        catch (SQLException Exception) {
            Exception.printStackTrace();
        }
    }
    
    @Override
    public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("stats")) { 
            String BukkitGet = Bukkit.getServer().getBukkitVersion();
            s.sendMessage(ChatColor.YELLOW+"=============== "+ChatColor.RED+""+ChatColor.BOLD+"[ROC] "+ChatColor.RESET+ChatColor.UNDERLINE+"Server Statistics"+ChatColor.YELLOW+" =============== ");
            s.sendMessage(ChatColor.GRAY+"Bukkit Version: "+ChatColor.GOLD+BukkitGet);
            s.sendMessage(ChatColor.GRAY+"Processor Cores Available (3.3ghz) : " 
            +ChatColor.GOLD+Runtime.getRuntime().availableProcessors());
            s.sendMessage(ChatColor.GRAY+"Free memory (GB): " + 
            ChatColor.GOLD+(Runtime.getRuntime().freeMemory()/1024)/1024/1024);
            long maxMemory = (Runtime.getRuntime().maxMemory()/1024)/1024/1024;
            s.sendMessage(ChatColor.GRAY+"Maximum Memory (GB): " + 
            ChatColor.GOLD+(maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));
            s.sendMessage(ChatColor.GRAY+"Memory Free for JVM: " + 
            ChatColor.GOLD+(Runtime.getRuntime().totalMemory()/1024)/1024/1024);
            File[] roots = File.listRoots();
            for (File root : roots) {
                s.sendMessage(ChatColor.GRAY+"File system root: " + ChatColor.GOLD+root.getAbsolutePath());
                s.sendMessage(ChatColor.GRAY+"Total space (bytes): " + ChatColor.GOLD+root.getTotalSpace());
                s.sendMessage(ChatColor.GRAY+"Free space (bytes): " + ChatColor.GOLD+root.getFreeSpace());
                s.sendMessage(ChatColor.GRAY+"Usable space (bytes): " + ChatColor.GOLD+root.getUsableSpace());        
            }
            return true;
    }
    else    {
        return false;
    }
}
}
