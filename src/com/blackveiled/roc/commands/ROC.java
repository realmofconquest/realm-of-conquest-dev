package com.blackveiled.roc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.Plugin;

import org.bukkit.entity.Player;
import org.bukkit.ChatColor;

public class ROC implements CommandExecutor {
    
    protected Plugin plugin;
    
    public ROC(Plugin plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
        if(s instanceof Player) {
            Player p = (Player) s;
            String playerName = p.getName();
            ///////////////////////////////////////////////////
            //////
            //////  USAGE:              /roc OR /roc reload
            //////  PURPOSE:            To show plugin version and name / To reload the plugin
            //////  PERMISSION-NODE:    roc.reload
            //////
            //////////////////////////////////////////////////
            if(cmd.getName().equalsIgnoreCase("roc")) { 
                if(args.length > 0) {
                    plugin.getPluginLoader().disablePlugin(this.plugin);
                    plugin.getPluginLoader().enablePlugin(this.plugin);
                    return true;
                }   else {
                    p.sendMessage(ChatColor.BOLD+""+ChatColor.LIGHT_PURPLE+"[ROC] "+ChatColor.RESET+ChatColor.UNDERLINE+"Realm of Conquest");
                    p.sendMessage("");
                    p.sendMessage(ChatColor.GRAY+"Plugin developed by: "+ChatColor.GOLD+"Blackveil");
                    p.sendMessage(ChatColor.GRAY+"Plugin Version: "+ChatColor.GOLD+"1.00a");
                    return true;
                }
            }
            ///////////////////////////////////////////////////
            //////
            //////  USAGE:              /warp <action> <category> <name>
            //////  PURPOSE:            To implement a database powered Warp system.
            //////  PERMISSION-NODE:    roc.warp, roc.setwarp, roc.delwarp
            //////
            //////////////////////////////////////////////////
            else {
                return false;
            }
        }
        return false;
    }
}
