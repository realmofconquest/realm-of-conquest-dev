/**
 *
 * @author Blackveiled
 */
package com.blackveiled.roc;
// Revision 1
import com.blackveiled.roc.commands.*;
import com.blackveiled.roc.player.PlayerEventListener;
import com.blackveiled.roc.sql.Database;
import com.blackveiled.roc.extra.ColorSigns;
import com.blackveiled.roc.items.ItemListener;
import com.blackveiled.roc.items.Builder;
import java.util.logging.Level;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.PluginLogger;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.ChatColor;

public class RealmOfConquest extends JavaPlugin {
    
    public Plugin plugin = this;
    public PluginManager pm = this.getServer().getPluginManager();
    public PluginLogger Logger = new PluginLogger(plugin);
    Database RoCDatabase = new Database();
    World Dungeons = null;
    
    @Override
    public void onEnable() {
        try {
            RoCDatabase.getConnection();
        }
        catch (SQLException Exception)  {
            Bukkit.broadcastMessage(ChatColor.RED+""+ChatColor.BOLD+"[SEVERE] Server not connected to the database!");
        }
        Logger.log(Level.INFO, "Realm of Conquest started!");
        pm.registerEvents(new PlayerEventListener(plugin), plugin);
        pm.registerEvents(new ColorSigns(), plugin);
        pm.registerEvents(new ItemListener(), plugin);
        Dungeons = Bukkit.createWorld(WorldCreator.name("Dungeons"));
        Dungeons.setAutoSave(true);
        //  REGISTER COMMANDS
        getCommand("roc").setExecutor(new ROC(plugin));
        getCommand("warp").setExecutor(new Warp(plugin));
        getCommand("stats").setExecutor(new Stats(plugin));
        //getCommand("tp").setExecutor(new tp());
        if(RoCDatabase.connection!=null)    {        
        try {
            RoCDatabase.getConnection();
            RoCDatabase.clearPlayersOnline();
            RoCDatabase.reloadPlayersOnline();
            Bukkit.broadcastMessage("Realm of Conquest Server Core has successfully reloaded!");
            Bukkit.broadcastMessage(ChatColor.GREEN+"The server currently has "+ChatColor.GOLD+""+RoCDatabase.getPlayersOnline()+""+ChatColor.GREEN+" players online.");
        }
        
        catch (SQLException Exception)  {
            Exception.printStackTrace();
        }
        }
        else    {
            try {
                RoCDatabase.getConnection();
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
            
        }
    }
    
    @Override
    public void onDisable() {
        if(RoCDatabase.connection!=null)    {        
            try {
                RoCDatabase.getConnection();
                RoCDatabase.clearPlayersOnline();
                RoCDatabase.connection.close();
            }
            catch (SQLException Exception)  {
                Exception.printStackTrace();
            }
            finally {
                System.gc();
            }
        }
    }
} // Class Ends
